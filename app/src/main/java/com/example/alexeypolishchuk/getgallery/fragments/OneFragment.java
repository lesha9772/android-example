package com.example.alexeypolishchuk.getgallery.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexeypolishchuk.getgallery.R;
import com.example.alexeypolishchuk.getgallery.adapters.MySimpleArrayAdapter;
import com.example.alexeypolishchuk.getgallery.components.ListItem;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class OneFragment extends android.support.v4.app.Fragment {
    ListView listView;
    ArrayList<ListItem> imageList = new ArrayList<ListItem>();
    String[] planets = new String[]{"Apple", "Banana", "Mango", "Grapes"
    };

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_one, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//
//        ArrayAdapter adapter = new ArrayAdapter(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, planets);

        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getActivity(), imageList);

        Bitmap bitm = BitmapFactory.decodeResource(getResources(), R.drawable.android);


        for (int i = 0; i < 100; i++) {
            imageList.add(new ListItem(bitm, "some_text_"+i, bitm));
        }



        listView = (ListView) getActivity().findViewById(R.id.listView);
        listView.setAdapter(adapter);
        // Assign adapter to ListView
//        listView.setAdapter(adapter);

        setKlickListener();

    }


    private void setKlickListener() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                int itemPosition = position;
                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
                // Show Alert
                Toast.makeText(getActivity(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}