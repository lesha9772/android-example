package com.example.alexeypolishchuk.getgallery.components;

import android.graphics.Bitmap;

/**
 * Created by alexeypolishchuk on 5/9/17.
 */

public class ListItem {
    public String text;
    public Bitmap image1, image2;

    public ListItem(Bitmap image1_, String text_, Bitmap image2_) {
        image1 = image1_;
        text = text_;
        image2 = image2_;
    }
}
