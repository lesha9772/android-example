package com.example.alexeypolishchuk.getgallery.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alexeypolishchuk.getgallery.R;
import com.example.alexeypolishchuk.getgallery.components.ListItem;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static android.R.attr.bitmap;


/**
 * Created by alexeypolishchuk on 5/9/17.
 */

public class MySimpleArrayAdapter extends BaseAdapter {
    private final Context context;
    View rowView;
    ImageView imageView1,imageView2 ;
    TextView text;
    ArrayList<ListItem> items;
    private ProgressDialog simpleWaitDialog;

    public MySimpleArrayAdapter(Context context, ArrayList<ListItem> items) {
//        super(context, -1);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        text = (TextView) rowView.findViewById(R.id.text);
        imageView1 = (ImageView) rowView.findViewById(R.id.image1);
        imageView2 = (ImageView) rowView.findViewById(R.id.image2);
        ListItem userItem = items.get(position);

        text.setText(userItem.text);
        imageView1.setImageBitmap(userItem.image1);
        imageView2.setImageBitmap(userItem.image2);

        return rowView;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
