package com.example.alexeypolishchuk.getgallery.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexeypolishchuk.getgallery.R;
import com.example.alexeypolishchuk.getgallery.adapters.ImageAdapter;
import com.example.alexeypolishchuk.getgallery.components.RecyclerItemClickListener;
import com.example.alexeypolishchuk.getgallery.components.TextRecyclerAdapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class TwoFragment extends android.support.v4.app.Fragment {
    List<String> dataSource;
    ArrayAdapter<String> adapter;
    View rootView;
    RecyclerView recyclerView;
    RecyclerView.LayoutParams lp;
    GridLayoutManager gridLayoutManager;


    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.fragment_two, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        dataSource = new ArrayList<String>();
        for (int i = 0; i < 100; i++) {
            dataSource.add(Integer.toString(i)+"_text");
        }
        gridLayoutManager = new GridLayoutManager(getActivity(), 8, GridLayoutManager.HORIZONTAL, false);

        recyclerView.setAdapter(new TextRecyclerAdapter(dataSource));
        recyclerView.setLayoutManager(gridLayoutManager);

        clickListenersGridView();
        return rootView;
    }

    private void clickListenersGridView() {

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        int itemPosition = position;

                        String itemValue = getItem(position);

//                        String itemValue = (String) getItem(position);
                        // TODO Handle item click
                        Toast.makeText(getActivity(),
                                "Position :" + itemPosition + "  GridItem : " + itemValue, Toast.LENGTH_SHORT)
                                .show();
                    }
                })
        );

    }



    public String getItem(int position) {

        return dataSource.get(position);
    }


}
