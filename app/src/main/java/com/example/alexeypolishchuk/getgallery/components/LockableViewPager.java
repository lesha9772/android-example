package com.example.alexeypolishchuk.getgallery.components;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by alexeypolishchuk on 5/30/17.
 */

public class LockableViewPager extends ViewPager {
    private boolean swipe;

    public LockableViewPager(Context context) {
        super(context);
    }

    public LockableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.swipe = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.swipe) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.swipe) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setSwipe(boolean swipe) {
        this.swipe = swipe;
    }
}