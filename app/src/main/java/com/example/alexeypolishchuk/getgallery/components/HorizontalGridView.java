package com.example.alexeypolishchuk.getgallery.components;

/**
 * Created by alexeypolishchuk on 5/30/17.
 */
import android.content.Context;
import android.widget.GridView;


public class HorizontalGridView extends GridView {

    public HorizontalGridView(Context context) {
        super(context);
    }
}
