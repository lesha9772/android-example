package com.example.alexeypolishchuk.getgallery.fragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.example.alexeypolishchuk.getgallery.R;


public class TwoFragmentOld extends android.support.v4.app.Fragment {

    String[] data = {
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"

    };

    GridView gridview;
    ArrayAdapter<String> adapter;
    View myFragmentView;
    RecyclerView recyclerView;
    GridLayoutManager gridLayoutManager;

    public TwoFragmentOld() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        myFragmentView = inflater.inflate(R.layout.fragment_two, container, false);
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.item, R.id.tvText, data);
        gridview = (GridView) myFragmentView.findViewById(R.id.gridview);

        // get the reference of RecyclerView
        recyclerView = (RecyclerView) myFragmentView.findViewById(R.id.recyclerView);
        // set a GridLayoutManager with default vertical orientation and 3 number of columns
        gridLayoutManager = new GridLayoutManager(getActivity(),3);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL); // set Horizontal Orientation
//        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        gridview.setAdapter(adapter);


//        gridview.setAdapter(adapter);
        adjustGridView();
        clickListenersGridView();

        // Inflate the layout for this fragment
        return myFragmentView;
    }

    private void adjustGridView() {
        gridview.setNumColumns(GridView.AUTO_FIT);
        gridview.setColumnWidth(100);
        gridview.setVerticalSpacing(5);
        gridview.setHorizontalSpacing(5);
        gridview.setStretchMode(GridView.STRETCH_SPACING_UNIFORM);
    }

    private void clickListenersGridView() {
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                int itemPosition = position;
                // ListView Clicked item value
                String itemValue = (String) gridview.getItemAtPosition(position);
                // Show Alert
                Toast.makeText(getActivity(),
                        "Position :" + itemPosition + "  GridItem : " + itemValue, Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }

}
